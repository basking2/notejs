const fs = require('fs')

class BuildIndexPlugin {
  constructor() {
    this.text = '';
  }

  apply(compiler){
    compiler.hooks.afterEmit.tapAsync('FileListPlugin', (compilation, callback) => {

      compilation.getAssets().forEach((v, i) => {
        var f = v['source']['existsAt']
        if (f.endsWith('.js')) {
          console.info(`Loading ${f}.`)
          f = fs.readFileSync(f)+";\n"
          this.text += f
        }
      })

      var index_html = fs.readFileSync('src/index.html').toString()
      var index_css = fs.readFileSync('src/index.css').toString()

      fs.open('dist/index.html', 'w', (err, fd) => {
        fs.writeSync(fd, 
          index_html.
            replace('{NOTEJS_SCRIPT}', this.text).
            replace('{NOTEJS_CSS}', index_css)
          )
      })

      callback()
    })
  }
}

module.exports = {
  mode: 'production',
  entry: {
    main: './src/index.js'
  },
  plugins: [
    new BuildIndexPlugin()
  ],
}

// vim: set et sw=2 :
