const settings = require('./settings')

function onChangeTextField(model, e) {
    settings.setPath(notejs_data, model, e.srcElement.value)
}

/**
 * 
 * @param {String} model Dot separated path to where the element's value is set.
 */
function mkTextField(model) {
    var a = document.createElement('input')
    a.type = 'text'
    a.value = settings.getPath(notejs_data, model) || ''
    a.onchange = (e) => {
      settings.setPath(notejs_data, model, e.srcElement.value)
    }
  
    return a
}

/**
 * Build a copy of this document as a string.
 */
function encodeHtml() {

    document.getElementById("notejs_data").innerText = "var notejs_data = "+JSON.stringify(notejs_data)

    var v = "<!DOCTYPE html>\n"
    v += "<html><head>\n"
    v += document.head.innerHTML
    v += "</head><body>"
    v += document.body.innerHTML
    v += "</body></html>"
    return v
}

/**
 * Call encodeHtml() and set it to the src of an iframe.
 */
function onClickSave(e) {

    var v = btoa(encodeHtml())
    var d = `data:application/octet-stream;base64,${v}`
    var iframe_nav = document.createElement("iframe")
    iframe_nav.style = "display: none;"    
    iframe_nav.src = d
    document.body.appendChild(iframe_nav)

    setTimeout(() => {
        document.body.removeChild(iframe_nav)
    }, 60000)

    return false;
}

function mkSaveLink(text) {
    var a = document.createElement('a')
    a.href = '#'
    a.onclick = (e) => {
        onClickSave()
        return false
    }

    a.innerText = text
    return a
}

module.exports = {
    mkTextField,
    encodeHtml,
    mkSaveLink,
    onClickSave,
    onChangeTextField
}