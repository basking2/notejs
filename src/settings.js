
/**
 * Set a JSON path in an object.
 * 
 * @param {Object} hash - Where data is stored in.
 * @param {String} path - A dot-separated path of object members.
 * @param {String} value - The value to set the path to.
 */
function setPath(hash, path, value) {
    var arr = path.split('.')
    var lastElement = arr.length-1
  
    arr.forEach((element, idx) => {
  
      if (idx == lastElement) {
        // Last element. Insert.
        hash[element] = value
      } else {
        if (element in hash === false) {
          // If there is no hash, make a hash.
          hash[element] = {}
        }
  
        hash = hash[element]
      }
    })
  }
  
  /**
   * Find an object in a path. Returns undefined if no key is found.
   * 
   * @param {Object} hash 
   * @param {String} path Dot separated path.
   */
  function getPath(hash, path) {
  
    for (var e in path.split('.')) {
      if (e in hash === false) {
        return undefined
      }
  
      hash = hash[element]
    }
  
    return hash
  }

  module.exports = {
      getPath,
      setPath
  }